import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TodoClass } from '../models/todo.model';

@Injectable({
  providedIn: 'root'
})
export class TodoApiService {

  constructor(private http:HttpClient) { }

  private URL = '/api/Todo/';

  GETData()
  {
    return this.http.get<TodoClass[]>(this.URL);
  }

  POSTData(item: TodoClass)
  {
    return this.http.post(this.URL,item);
  }

  UPDATEData(ID:string, item:TodoClass)
  {
    return this.http.put(this.URL  + ID,item);
  } 

  DELETEData(ID: string)
  {
    return this.http.delete(this.URL + ID);
  }

  DELETEMultiData(IDs:string)
  {
    return this.http.delete(this.URL+'DeleteCompleted/'+IDs);
  }

  SELECTALL(toogleAll :boolean)
  {
    return this.http.put(this.URL+'TogoleAll/'+toogleAll,null)
  }

}
