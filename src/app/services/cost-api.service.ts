import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Record } from '../models/account.model';
@Injectable({
  providedIn: 'root'
})
export class CostApiService {

  constructor(private http:HttpClient) { }

  private URL = '/api/Cost/'

  GETData()
  {
    return this.http.get<Record[]>(this.URL);
  }

  POSTData(item: Record)
  {
    return this.http.post(this.URL, item);
  }

  DELETEData(ID: string)
  {
    return this.http.delete(this.URL + ID);
  }
}
