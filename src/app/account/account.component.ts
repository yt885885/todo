import { CostApiService } from '../services/cost-api.service';
import { Record } from './../models/account.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  constructor(private costApiService:CostApiService){}
  
  ngOnInit(): void {
    this.LoadRecord();
  }

  description: string = '';
  amount: number = 0;
  date: string = '';
  records :Record[] = [];

  addRecord()
  {
    if (this.description && this.amount !== 0 && this.date!== '' ) {
      let addItem : Record = {
        id:"",
        description:this.description,
        cost:this.amount,
        date:this.date
      }
      this.costApiService.POSTData(addItem).subscribe(
        {
          next: () => {
            this.description = '';
            this.amount = 0;
            this.date = '';
            this.LoadRecord(); // 成功後刷新數據
          },
          error: (err) => console.error('Error adding item:', err)
        }
      );
    }
  }

  deleteRecord(id:string)
  {
    this.costApiService.DELETEData(id).subscribe(
      {
        next: () => {
          this.LoadRecord(); // 成功後刷新數據
        },
        error: (err) => console.error('Error delete item:', err)
      }
    );
  }

  LoadRecord()
  {
    this.costApiService.GETData().subscribe({
      next: (data) => {
        this.records = data;
        // this.toogleall = this.getCompleted.length === this.todoList.length;
      },
      error: (err) => console.error('Error fetching data:', err)
    });
  }
  get getTotalCost(): number {
    return this.records.reduce((total, record) => total + record.cost, 0);
  }
  
}
