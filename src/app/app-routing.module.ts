import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NotfoundComponent } from './notfound/notfound.component';


const routes: Routes = [
    // { path:'login', component:LoginComponent},
    { path:'login', loadChildren :() => import('./login/login.module').then(m => m.LoginModule)},
    // { path:'manage', component:ManageComponent,
    //   children:[
    //   { path:'todo', component:TodoComponent},
    //   { path:'home', component:HomeComponent},
    //   { path:'',redirectTo:'home', pathMatch:'full'},
    // ]},
    { path:'manage', loadChildren :() => import('./manage/manage.module').then(m => m.ManageModule)},
    { path:'',redirectTo:'/login', pathMatch:'full'},
    { path:'**', component:NotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }