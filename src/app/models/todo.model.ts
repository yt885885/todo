// interface 用法
export interface Todo{
    Thing: string;
    Status: boolean;
}

// class 用法
export class TodoClass{
    // Status:boolean
    // Thing:string
    // Editing:boolean
    status:boolean
    thing:string
    Editing:boolean
    id:string
    constructor(thing: string, status:boolean = false, Editing:boolean=false, id:string)
    {
        this.thing = thing
        this.status = status
        this.Editing = Editing
        this.id = id
    }
 
    togole()
    {
        this.status = !this.status
    }
}

export enum TodoStatusType
{
    All,
    Active,
    Completed
}