// interface 用法
export interface Record{
    id:string,
    description: string;
    cost: number;
    date: string;
}