import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageRoutingModule } from './manage-routing.module';
import { ManageComponent } from './manage.component';
import { TodoComponent } from '../todo/todo.component';
import { MenuComponent } from '../menu/menu.component';
import { HeaderComponent } from '../header/header.component';
import { HomeComponent } from '../home/home.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ManageComponent,
    // TodoComponent,
    MenuComponent,
    HeaderComponent,
    HomeComponent,

    ],
  imports: [
    CommonModule,
    ManageRoutingModule,
    FormsModule, 
  ]
})
export class ManageModule { }
