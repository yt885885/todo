
import { HttpClient } from '@angular/common/http';
import { Todo, TodoClass,TodoStatusType } from '../models/todo.model';
import { Component, OnInit } from '@angular/core';
import { TodoApiService } from '../services/todo-api.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent {
  title = 'Allen todo';
  placeholder="What needs to be done?"
  toogleall = false;
  check1 = false;
  check2 = false;
  complete = '';
  newItem = "";

  todoStatusType = TodoStatusType.All;
  statusType = TodoStatusType;
  inputItem = '';

  todoList:TodoClass[]  = []

  constructor(private todoApiService:TodoApiService){}
  
get RefreshItem()
  {
    let filter: TodoClass[] = [];

    switch(this.todoStatusType)
    {
      case TodoStatusType.Active:
        filter = this.todoList.filter(data=> !data.status);
        break
      case TodoStatusType.Completed:
        filter = this.todoList.filter(data => data.status);
        break
      default:
        filter = this.todoList;
    }
    return filter;

  }

  get getActive():TodoClass[]
  {
    return this.todoList.filter(data=> !data.status)
  }

  get getCompleted():TodoClass[]
  {
    return this.todoList.filter(data=> data.status)
  }

  EditClick(item : TodoClass)
  {
    item.Editing = true;
  }

  setStatus(type:number)
  {
    this.todoStatusType = type;
  }

  ngOnInit(): void {
    this.todoApiService.GETData().subscribe(data => {
      this.todoList = data;
    })
  }

  RefreshData()
  {
    this.todoApiService.GETData().subscribe({
      next: (data) => {
        this.todoList = data;
        // this.toogleall = this.getCompleted.length === this.todoList.length;
      },
      error: (err) => console.error('Error fetching data:', err)
    });
  }

  // AddItem(input:HTMLInputElement)
  AddItem()
  {
      // this.todoList.push({
      //   Status:false,
      //   Thing:input.value
      // });

      // let obj: Todo ={
      //   Thing:input.value,
      //   Status : false
      // }
      // this.todoList.push(obj);
      
      // this.todoList.push(new TodoClass(this.inputItem,false,false,""));
      let addItem : TodoClass = new TodoClass(this.inputItem,false,false,"")
      // this.todo.post('/api/Todo',addItem).subscribe();
      this.todoApiService.POSTData(addItem).subscribe(
        {
          next: () => {
            this.inputItem = '';
            this.RefreshData(); // 成功後刷新數據
          },
          error: (err) => console.error('Error adding item:', err)
        }
      );
      // this.inputItem = '';
      // this.RefreshData();
  }

  selectAll()
  {
    this.toogleall = !this.toogleall;

    // this.todoList.forEach(item => {
    //   item.status = this.toogleall;
    // })
    this.todoApiService.SELECTALL(this.toogleall).subscribe(
      {
        next: () => {
          this.RefreshData(); // 成功後刷新數據
        },
        error: (err) => console.error('Error selectAll item:', err)
      }
    );
    this.RefreshData();
  }

  checked(item : TodoClass)
  {
    item.status = !item.status;
    this.updateItem(item);
    if(this.getCompleted.length === this.todoList.length)
    {
      this.toogleall = true;
    }
    else
    {
      this.toogleall = false;
    }
  }

  updateItem(item: TodoClass)
  {
    this.todoApiService.UPDATEData(item.id, item).subscribe(
      {
        next: () => {
          item.Editing = false;
          this.toogleall = this.getCompleted.length === this.todoList.length;
          this.RefreshData(); // 成功後刷新數據
        },
        error: (err) => console.error('Error update item:', err)
      }
    );
    // item.Editing = false;
    // this.RefreshData();
  }

  delete(item:TodoClass)
  {
    //delete element
    // this.todoList.splice(index,1);
    // this.todoList = this.todoList.filter(data => data != item)
    // this.http.delete('/api/Todo/' + item.id).subscribe();
    this.todoApiService.DELETEData(item.id).subscribe(
      {
        next: () => {
          this.RefreshData(); // 成功後刷新數據
        },
        error: (err) => console.error('Error delete item:', err)
      }
    );
    this.RefreshData();
  }

  ClearCompleted()
  {
    let ids = '';
    this.todoList.forEach(data=>{
      if(data.status)
        {
          ids = ids + ',' + data.id
        }
    })
    ids = ids.substring(1)
    this.todoList = this.getActive;
    this.todoApiService.DELETEMultiData(ids).subscribe(
      {
        next: () => {
          this.RefreshData(); // 成功後刷新數據
        },
        error: (err) => console.error('Error delete item:', err)
      }
    );
  }

}
