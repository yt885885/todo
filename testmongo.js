const mongoose = require('mongoose');

// MongoDB 連接設置
const dbURI = 'mongodb://mongodb:27017/TODO'; // 替換成您的 MongoDB 連接字符串

// 連接 MongoDB
mongoose.connect(dbURI)
  .then(() => {
    console.log('MongoDB connected');
    // 在這裡可以進行其他操作，如定義模型、查詢數據等
  })
  .catch(err => {
    console.error('MongoDB connection error:', err);
  });